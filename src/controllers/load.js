var mongoose = require('mongoose')
  , User = require('../models/user')
  , Pair = require('../models/pair')
  , decrypt = require('../utils').decrypt;

mongoose.Promise = global.Promise;

module.exports = function($) {
  var globalUser = {}
    , globalResult = {};
  User.findOne({
    telegramId: $.user.id
  }).exec().then(function(user) {
    if (!user) {
      throw {user: true}
    }
    globalUser = user;
    var form = {
      key: {
        q: 'Ключ пароля?',
        validator: function(input, callback) {
          callback(true);
        }
      }
    }
    return new Promise(function(resolve, reject) {
      $.runForm(form, function(result) {
        resolve(result);
      });
    });
  }).then(function(result) {
    globalResult = result
    return Pair.findOne({
      user: globalUser.id,
      key: result.key
    }).exec();
  }).then(function(pair) {
    if (!pair) {
      throw { pair: true }
    }

    $.sendMessage('Требуемый пароль — ' + decrypt(pair.password, globalUser.masterKey));
  }).catch(function(err) {
    console.log(err);
    if (err.user) {
      $.sendMessage('Я не знаю тебя. Расскажи о себе.')
    }
    if (err.pair) {
      $.sendMessage('Пароль с таким ключом не найден.')
    }
  })
};
