var mongoose = require('mongoose')
  , User = require('../models/user')
  , Pair = require('../models/pair')
  , encrypt = require('../utils').encrypt;

mongoose.Promise = global.Promise;

module.exports = function($) {
  var globalUser = {}
    , globalResult = {};
  User.findOne({
    telegramId: $.user.id
  }).exec().then(function(user) {
    if (!user) {
      throw {user: true}
    }
    globalUser = user;
    var form = {
      key: {
        q: 'Ключ пароля?',
        validator: function(input, callback) {
          callback(true);
        }
      },
      password: {
        q: 'Пароль?',
        validator: function(input, callback) {
          callback(true);
        }
      }
    }
    return new Promise(function(resolve, reject) {
      $.runForm(form, function(result) {
        resolve(result);
      });
    });
  }).then(function(result) {
    globalResult = result
    return Pair.findOne({
      user: globalUser.id,
      key: result.key
    }).exec();
  }).then(function(pair) {
    if (pair) {
      throw { pair: true }
    }

    var pwd = encrypt(globalResult.password, globalUser.masterKey);
    var newPair = new Pair({
      user: globalUser,
      key: globalResult.key,
      password: pwd
    });
    return newPair.save();
  }).then(function(pair) {
    $.sendMessage('Я сохраню этот пароль.')
  }).catch(function(err) {
    console.log(err);
    if (err.user) {
      $.sendMessage('Я не знаю тебя. Расскажи о себе.')
    }
    if (err.pair) {
      $.sendMessage('Пароль с таким ключом уже хранится тут.')
    }
  })
};
