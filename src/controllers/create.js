var mongoose = require('mongoose')
  , User = require('../models/user');

mongoose.Promise = global.Promise;
module.exports = function($) {

  User.findOne({
    telegramId: $.user.id
  }).exec().then(function(user) {
    if (user) {
      return user;
    }
    var form = {
      masterKey: {
        q: 'Напиши свой ключ.',
        validator: function(input, callback) {
          callback(true);
        }
      }
    }
    return new Promise(function(resolve, reject) {
      $.runForm(form, function(result) {
        resolve(result);
      });
    });
  }).then(function(result) {
    if (result.telegramId) {
      $.sendMessage('Я знаю тебя, ' + result.name + '.')
      return;
    }
    var user = new User({
      telegramId: $.user.id,
      name: $.user.first_name,
      masterKey: result.masterKey
    })
    return user.save();
  }).then(function(user) {
    if (!user) {
      return;
    }
    $.sendMessage('Привет '+ user.name + '. Я запомню что ты рассажешь мне.')
  }).catch(function(err) {
    console.log(err);
  });
};
