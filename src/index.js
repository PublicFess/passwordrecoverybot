'use strict';

var tg = require('telegram-node-bot')('162328782:AAHzAKJQwhu-kjSe-wn1yx_jJl8P2JgBELk')
  , mongoose = require('mongoose')
  , User = require('./models/user')
  , Pair = require('./models/pair')

  , createController = require('./controllers/create.js')
  , saveController = require('./controllers/save.js')
  , loadController = require('./controllers/load.js') ;

mongoose.connect('mongodb://localhost/passwordRecoveryBot');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  tg.router
    .when(['create'], 'CreateController')
    .when(['save'], 'SaveController')
    .when(['load'], 'LoadController');

  tg.controller('CreateController', createController);
  tg.controller('SaveController', saveController);
  tg.controller('LoadController', loadController);

});
