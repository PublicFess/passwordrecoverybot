"use strict";
var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
  telegramId: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  masterKey: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('User', userSchema);
